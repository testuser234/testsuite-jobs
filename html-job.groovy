def report_directory = 'reports/*.xml'
def reportResult(directory) {
  junit directory
}
node {
  stage("Clone") {
    git "https://gitlab.com/testuser234/testsuite.git"
  }
  stage("Test") {
    try {
      sh "mkdir -p reports"
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
        sh "behave --junit --junit-directory reports"
      }
    } catch(err) {
      reportResult(report_directory)
      error "${err}"
    }
  }
  stage("Report") {
    reportResult(report_directory)
  }
}